//
//  main.swift
//  BelajarOOP
//
//  Created by Taufiq Ramadhany on 28/04/20.
//  Copyright © 2020 Taufiq Ramadhany. All rights reserved.
//

import Foundation

let someResolution = Resolution()
let someVideoMode = VideoMode()

print("The width of someResolution is \(someResolution.width)")
// Prints "The width of someResolution is 0"

print("The width of someVideoMode is \(someVideoMode.resolution.width)")
// Prints "The width of someVideoMode is 0"



