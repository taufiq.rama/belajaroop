//
//  VideoMode.swift
//  BelajarOOP
//
//  Created by Taufiq Ramadhany on 28/04/20.
//  Copyright © 2020 Taufiq Ramadhany. All rights reserved.
//

import Foundation

class VideoMode {
    var resolution = Resolution()
    var interlaced = false
    var frameRate = 0.0
    var name: String?
}
