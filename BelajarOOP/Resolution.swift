//
//  Resolution.swift
//  BelajarOOP
//
//  Created by Taufiq Ramadhany on 28/04/20.
//  Copyright © 2020 Taufiq Ramadhany. All rights reserved.
//


import Foundation

/**
ini struct of Resolution
*/

struct Resolution {
    var width = 0
    var height = 0
}
